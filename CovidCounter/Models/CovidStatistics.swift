//
//  CovidStatistics.swift
//  CovidCounter
//
//  Created by Михаил Тененёв on 28.04.2020.
//  Copyright © 2020 Михаил Тененёв. All rights reserved.
//

import SwiftUI

final class CovidStatistics: ObservableObject {
    
    @Published var ConfirmedCasesCount: Int = 0
    
    @Published var RecoveredCount: Int = 0
    
    @Published var DeathsCount: Int = 0
    
    init() {
        ConfirmedCasesCount = 1036054
        RecoveredCount = 12302;
        DeathsCount = 435
    }
    
}
