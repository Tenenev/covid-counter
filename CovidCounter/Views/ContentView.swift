//
//  ContentView.swift
//  CovidCounter
//
//  Created by Михаил Тененёв on 27.04.2020.
//  Copyright © 2020 Михаил Тененёв. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State private var showModal = false
    
    @EnvironmentObject var orientationInfo: OrientationInfo
    
    @EnvironmentObject var statistics: CovidStatistics
    
    var body: some View {
        VStack() {
            Text("Location:")
            Button("Saint Petersburg") {
                self.showModal.toggle()
            }.sheet(isPresented: $showModal) {
                LocationSelectModalView(showModal: self.$showModal)
            }
            Spacer()
            Text("Confirmed cases")
                .font(.system(size: 20))
            HStack {
                Spacer()
                Text(statistics.ConfirmedCasesCount.formattedWithDecimalSeparator)
                    .font(.system(size: (orientationInfo.orientation == .portrait ? 60 : 40)))
                    .padding(.top, (orientationInfo.orientation == .portrait ? -5 : 0))
                Spacer()
            }
            Text("Recovered")
                .font(.system(size: 20))
                .padding(.top, (orientationInfo.orientation == .portrait ? 30 : 20))
            HStack {
                Spacer()
                Text(statistics.RecoveredCount.formattedWithDecimalSeparator)
                    .font(.system(size: (orientationInfo.orientation == .portrait ? 60 : 40)))
                    .padding(.top, (orientationInfo.orientation == .portrait ? -5 : 0))
                Spacer()
            }
            Text("Deaths")
                .font(.system(size: 20))
                .padding(.top, (orientationInfo.orientation == .portrait ? 30 : 20))
            HStack {
                Spacer()
                Text(statistics.DeathsCount.formattedWithDecimalSeparator)
                    .font(.system(size: (orientationInfo.orientation == .portrait ? 60 : 40)))
                    .padding(.top, (orientationInfo.orientation == .portrait ? -5 : 0))
                Spacer()
            }
            Spacer()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(OrientationInfo())
            .environmentObject(CovidStatistics())
    }
}
