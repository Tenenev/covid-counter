//
//  ModalView.swift
//  CovidCounter
//
//  Created by Михаил Тененёв on 28.04.2020.
//  Copyright © 2020 Михаил Тененёв. All rights reserved.
//

import SwiftUI

struct LocationSelectModalView: View {
    
    @Binding var showModal: Bool
    
    var body: some View {
        VStack {
            Picker(selection: .constant(1), label: EmptyView()) {
                Text("Moscow").tag(1)
                Text("Saint Petersburg").tag(2)
            }
            .labelsHidden()
            Button("Confirm") {
                self.showModal.toggle()
            }
        }
    }
}

struct LocationSelectModalView_Previews: PreviewProvider {
    static var previews: some View {
        LocationSelectModalView(showModal: .constant(true))
    }
}
