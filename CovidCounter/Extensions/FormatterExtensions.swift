//
//  FormatterExtensions.swift
//  CovidCounter
//
//  Created by Михаил Тененёв on 28.04.2020.
//  Copyright © 2020 Михаил Тененёв. All rights reserved.
//

import Foundation

extension Formatter {
    static let withDecimalSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}
