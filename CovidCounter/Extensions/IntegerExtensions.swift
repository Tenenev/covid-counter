//
//  IntegerExtensions.swift
//  CovidCounter
//
//  Created by Михаил Тененёв on 28.04.2020.
//  Copyright © 2020 Михаил Тененёв. All rights reserved.
//

import Foundation

extension Int {
    var formattedWithDecimalSeparator: String {
        return Formatter.withDecimalSeparator.string(for: self) ?? ""
    }
}
